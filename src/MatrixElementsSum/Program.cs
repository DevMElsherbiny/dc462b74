﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MatrixElementSum
{
    public class Program
    {
        private static void Main()
        {
            var array = new[]
            {
                new[] { 0, 1, 1, 2 },
                new[] { 0, 5, 0, 0 },
                new[] { 2, 0, 3, 3 }
            };

            Console.WriteLine(GetMatrixElementsSum(array));
        }

        public static int GetMatrixElementsSum(int[][] matrix)
        {
            int sum = 0;

            var rowsLength = matrix.GetLength(0);
            var columnsLenght = matrix[0].Length;

            for (int column = 0; column < columnsLenght; column++)
                for (int row = 0; row < rowsLength; row++)
                {
                    if (matrix[row][column] != 0)
                        sum += matrix[row][column];
                    else
                        break;
                }
            return sum;
        }
    }
}
